# Deus Ex: Mankind Divided modding

This repo is dedicated to modding and reverse engineering DX:MD. It contains
part custom software and part documentation and RE hints.

## MDSuen

MDSuen is a work-in-progress modding tool for DX:MD that hooks into the game's
executable.

## Datautils

Datautils is a collection of scripts for processing DX:MD's data files.

## Reverse-engineering notes

### Binary with symbols

Fortunately, you can find a beta binary of the macOS version on Steam, which is
not stripped (i.e. contains symbols, names for functions, classes, etc.). To
download it, open the [Steam console](steam://open/console) and run the
following command:

	download_depot 337000 337013 5348531537588119210

After the download has finished, the binary will be in your Steam folder, in
`steamapps/content/app_337000/depot_337013/Deus Ex: Mankind
Divided™.app/Contents/MacOS`.

Unfortunately, the corresponsing data files can't be downloaded this way and
I couldn't get the executable to launch with newer files (it is probably
doable, though).

### Published documents

#### Primarily useful for modding

- [Assets and Entities in the Glacier 2 Engine](https://m.youtube.com/watch?v=qw7BhS1lMkI)
  Talk on how assets are processed ("packed") and loaded into the engine and on
  the many uses of entities.
- [Modular Sandbox Design: Tools and Workflows for Hitman](https://www.gdcvault.com/play/1022840/Modular-Sandbox-Design-Tools-and)
  Not actually applicable to DX:MD, but neat nonetheless
- [An In-Depth Look at the Sound Design Choices and Technical Solutions in Hitman: Absolution](https://www.gdcvault.com/play/1017967/An-In-Depth-Look-at)

#### Other related documents

- [Behind the scene - Building the world of Deus Ex: Mankind Divided](https://www.autodesk.com/autodesk-university/class/Behind-scene-Building-world-Deus-Ex-Mankind-Divided-2016)
- [GDC Vault: Improving Geometry Culling for Deus Ex Mankind Divided presented by Umbra](https://www.youtube.com/watch?v=CkMFDzNsWgw)
- [Informant Diegesis in Videogames](https://cujo.dk/downloads/InformantDiegesis.pdf)
- [Rendering Techniques in Deus Ex: Mankind Divided](https://www.slideshare.net/EidosMontreal/rendering-techniques-in-deus-ex-mankind-divided)
- [Sound Propagation in Hitman](https://www.gdcvault.com/play/1022824/Sound-Propagation-in)
- [Deus Ex: Breach': Experimenting Within the Boundaries of a AAA Franchise](https://www.gdcvault.com/play/1024074/-Deus-Ex-Breach-Experimenting)
- [Reimagining a Classic: The Design Challenges of Deus Ex: Human Revolution](https://www.gdcvault.com/play/1015845/Reimagining-a-Classic-The-Design) [slides](https://www.gdcvault.com/play/1015489/Reimagining-a-Classic-The-Design)
- [City of a Thousand Choices: Prague City Hub in 'Deus Ex Mankind Divided'](https://www.gdcvault.com/play/1024003/A-City-of-a-Thousand) [slides](https://www.gdcvault.com/play/1024502/A-City-of-a-Thousand)
- [Level Design Workshop: Rewarding Exploration in 'Deus Ex: Mankind
  Divided'](https://www.gdcvault.com/play/1024305/Level-Design-Workshop-Rewarding-Exploration) [slides](https://www.gdcvault.com/play/1024599/Level-Design-Workshop-Rewarding-Exploration)
- [Improving Geometry Culling for 'Deus Ex: Mankind Divided' (presented by Umbra)](https://www.gdcvault.com/play/1023678/Improving-Geometry-Culling-for-Deus)
- [Building the Story-driven Experience of Deus Ex: Human Revolution](https://www.gdcvault.com/play/1015027/Building-the-Story-driven-Experience) [slides](https://www.gdcvault.com/play/1015028/Building-the-Story-driven-Experience)
- [Creating a Unique Visual Direction: The Successes and Failures of Creating a Near-Future Cyberpunk Setting with a Renaissance Twist in Deus Ex 3](https://www.gdcvault.com/play/1012334/Creating-a-Unique-Visual-Direction)
- [Reinforcement Learning Based Character Locomotion in Hitman: Absolution](https://www.gdcvault.com/play/1017727/Reinforcement-Learning-Based-Character-Locomotion)
- Crowds in Hitman: Absolution [GDC Europe](https://www.gdcvault.com/play/1016443/Crowds-in-Hitman) [slides](https://www.gdcvault.com/play/1016518/Crowds-in-Hitman) [GDC](https://www.gdcvault.com/play/1015526/Crowds-in-Hitman) [slides](https://www.gdcvault.com/play/1015315/Crowds-in-Hitman)

### Feral launcher options

The launcher for the Linux and macOS versions of the game accepts some
options. Some can be used on the command line, some should be written into the
"Advanced options" field in the launcher.

- `-benchmark` launches the benchmark on startup
- `--show-performance` and `--no-show-performance` show and hide a performance
  overlay respectively
- `-showdebugpanel` Shows a tab with some debug options in the launcher
- `-showtestpanel` Shows a tab with some options for testers in the launcher

### General notes

DX:MD's engine ("Dawn Engine") is built on IOI's Glacier 2 engine. I can't
precisely say how much they have in common, but there are some parts left over
that still exist, but aren't really used in DX:MD. For example, the engine
supports an INI-like config file, but as far as I'm aware, it doesn't do
anything useful. There is also an in-game console for setting variables on
runtime, but it's only used for cheats and a few debug variables here.

Some functions don't conform to the standard calling convention, which makes
them harder to hook and call from MDSuen. I suspect that this is the result of
some compiler optimisation, which "half-inlines" the functions. (TODO: Find out
what it's called and how it works more precisely). Thankfully, only some
functions are affected. I have a suspicion that this is performed only on
somehow internal functions (maybe those that can't cross a compile unit
boundary?).

### Useful classes/functions

The game code calls the resource paths/names "resource IDs" and calls the
64-bit numeric IDs (partly hashes) "runtime resource IDs.
`ZRuntimeResourceID::QueryRuntimeResourceID` converts between the two. It also
stores a mapping between them in a global `LocalResourceIDsResolver`.

`ZResourceStub` seems to represent a loaded resource.

`ZResourcePtr<T>` is a generic class pointing to a resource. Its
`GetRawPointerInternal` gives a pointer to the resource's raw data.

`ZResourceManager` is a singleton that seems to have a central role in loading
resources.

In the Mac version, `WinMain` is the actual game's main function (after the
launcher). `ZEngineAppCommon::InitModules` can serve a useful primer on the
areas the code is divided into.
