// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include <iostream>
#include <args.hxx>
#include <pahil.hpp>

/** This is needed on Windows to give the user a chance to read the output */
void graceful_exit(int retcode) {
#if defined(_WIN32)
    std::cout << "Press enter to exit..." << std::endl;
    std::cin.get();
#endif
    exit(retcode);
}

int main(int argc, char *argv[]) {
#if defined(__linux__)
    std::string dxmd_path_default = "bin/DeusExMD";
    std::string hooker_path_default = "libmdsuen_lib.so";
#elif defined(_WIN32)
    std::string dxmd_path_default = "DXMD.exe";
    std::string hooker_path_default = "mdsuen_lib.dll";
#endif

    args::ArgumentParser parser("MDSuen launcher");
    parser.helpParams.addDefault = true;
    args::HelpFlag help(parser, "help", "Display help", {'h', "help"});
    args::ValueFlag<std::string> dxmd_path(
        parser, "path", "Path to game executable", {'g'}, dxmd_path_default);
    args::ValueFlag<std::string> hooker_path(
        parser, "path", "Path to hooker library", {'l'}, hooker_path_default);

    try {
        parser.ParseCLI(argc, argv);
    } catch(const args::Completion& e) {
        std::cout << e.what();
        graceful_exit(0);
    } catch(const args::Help&) {
        std::cout << parser;
        graceful_exit(0);
    } catch(const args::ParseError& e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        graceful_exit(1);
    }

    try {
        pahil::launch_with_lib(args::get(dxmd_path), args::get(hooker_path));
    } catch(std::exception& e) {
        std::cerr << "Launch failed!" << std::endl;
        std::cerr << e.what() << std::endl;
        graceful_exit(2);
    }

    std::cout << "Mankind Divided with MDSuen launched successfully!"
              << std::endl;

    // No need to hold the console open if everything went well
    return 0;
}
