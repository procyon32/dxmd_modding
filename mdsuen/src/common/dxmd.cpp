// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "dxmd.hpp"
#include <cstring>
#include <pahil/logging.hpp>

ZString ZString_from_cstr(const char *cstr) {
    auto cstr_len = strlen(cstr);
    char *cstr_new = new char[cstr_len + 1];
    memcpy(cstr_new, cstr, cstr_len + 1);
    ZString res;
    res.cstr = cstr_new;
    res.size = static_cast<uint32_t>(cstr_len);
    res.capacity = static_cast<uint32_t>(cstr_len); // TODO: +1?
    return res;
}

std::function<void *(const char *name)> GetGlobalPointer;
std::function<void(void *self, const char *cmd)> ZDebugConsole__ExecuteCommand;
std::function<void(ZString *transition)> TriggerMenuScreenTransition__string;
std::function<ID3D12Device *(nx::NxD3DImpl *)> NxD3DImpl__GetD3D12Device;
std::function<IDXGISwapChain1 *(nx::NxDXGIImpl *)> NxDXGIImpl__GetSwapChain;

void *g_pDebugConsoleSingleton = NULL;
uint32_t *globalPointerCount = NULL;
GlobalPointer *globalPointers = NULL;
NxApp_Globals **s_Globals;
