// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <stdint.h>
#include <functional>

#if defined(_WIN32)
#include <d3d12.h>
#include <dxgi1_3.h>
#endif

#include <pahil/logging.hpp>

struct ZString {
    char *cstr;
    uint32_t size;
    uint32_t capacity;
};

// TODO: Virtual class?
struct ZConfigCommand {
    void *vptr;
    ZString name;
    ZString description;
    ZConfigCommand *next;
};

ZString ZString_from_cstr(const char *cstr);

struct GlobalPointer {
    char *name;
    void *val;
};

template <typename T>
struct TArray {
    T *data;
    uint32_t size;
    uint32_t capacity;

    T *begin() {
        return this->data;
    }

    T *end() {
        return this->data + this->size;
    }
};

template <typename A, typename B>
struct TPair {
    A a;
    B b;
};

template <typename T>
struct THashMapNode {
    /** Points to next node with the same hash */
    uint64_t next_index;
    T val;
};

template <typename K, typename V>
class THashMapIterator {
    public:
    THashMapIterator(uint64_t *node_indices, size_t capacity,
                     THashMapNode<TPair<K, V>> *nodes,
                     size_t index, size_t index_in_hash)
        : node_indices(node_indices), capacity(capacity), nodes(nodes),
          index(index), index_in_hash(index_in_hash) {
        // If not deliberately trying to construct an out-of-bounds iterator,
        // skip any invalid nodes at the start
        if(index_in_hash != -1ul) {
            this->skip_to_valid();
        }
    }

    bool operator==(THashMapIterator<K, V>& other) {
        return
            this->node_indices == other.node_indices
         && this->nodes == other.nodes
         && this->index == other.index
         && this->index_in_hash == other.index_in_hash;
    }

    bool operator!=(THashMapIterator<K, V>& other) {
        return !this->operator==(other);
    }

    THashMapIterator<K, V>& operator++() {
        this->index_in_hash = this->nodes[this->index_in_hash].next_index;
        if(this->index_in_hash != -1ul) {
            return *this;
        }
        this->index_in_hash = 0;
        this->index++;
        this->skip_to_valid();

        return *this;
    }

    TPair<K, V>& operator*() {
        return this->nodes[this->index_in_hash].val;
    }

    private:
    uint64_t *node_indices;
    size_t capacity;
    THashMapNode<TPair<K, V>> *nodes;
    /** Index into node_indices */
    size_t index;
    /** Index into nodes */
    size_t index_in_hash;

    void skip_to_valid() {
        for(; this->index < this->capacity; this->index++) {
            if(this->node_indices[this->index] != -1ul) {
                this->index_in_hash = this->node_indices[this->index];
                break;
            }
        }
    }
};

template <typename K, typename V>
struct THashMap {
    uint64_t node_count;
    uint64_t unk1;
    uint64_t capacity;
    /** Indexed by hash % capacity */
    uint64_t *node_indices;
    THashMapNode<TPair<K, V>> *nodes;

    THashMapIterator<K, V> begin() {
        return THashMapIterator(
            this->node_indices, this->capacity, this->nodes, 0, 0);
    }
    THashMapIterator<K, V> end() {
        return THashMapIterator(
            this->node_indices, this->capacity, this->nodes,
            this->capacity - 1, 0);
    }
};

struct ZSpinlock {
    char unk1[32]; // TODO
};

struct STypeID;

struct TypeInfoProperty {
    uint32_t name_crc32;
    uint32_t unk1;
    STypeID *stypeid;
    uint64_t unk2;
    uint64_t unk3;
    void *set_callback_func;
    uint64_t unk4;
};

struct TypeInfoInputPin {
    const char *name;
    uint64_t unk1;
    STypeID *stypeid;
    void *functor; // ? Some kind of function, maybe "setter"?
    uint64_t unk2;
    uint64_t unk3;
};

struct IType {
    // Signifies that this IType has properties, constructors...
    // This name isn't great, but I don't know how else to group them
    static const uint16_t FLAG_IS_CLASS = 4;

    void *type_functions;
    uint16_t instance_size;
    uint8_t alignment; // ?
    uint8_t padding1;
    uint16_t flags;
    uint16_t padding2;
    const char *name;
    STypeID *stypeid;
    // Can return by reference or by value
    void *(*FromString)(void *, IType *, ZString *);
    void (*ToString)(void *, IType *, char *, uint64_t, ZString *);
    uint16_t property_count;
    uint16_t constructor_count;
    uint16_t base_class_count;
    uint16_t component_map_len;
    uint16_t input_pin_count;
    uint8_t padding3[3];
    TypeInfoProperty *properties;
    /** Array of function pointers to virtual methods of TConstructorCall */
    void *constructors;
    STypeID **base_classes;
    struct {
        STypeID *stypeid;
        uint64_t unk1;
    } *component_map;
    TypeInfoInputPin *input_pins;
};

struct STypeID {
    // Commonly used as if(flags & 5) typeid = typeid.inner
    uint64_t flags; // TODO
    IType *details;
    STypeID *inner;
};

struct ZTypeRegistry {
    void *vptr;
    ZSpinlock lock;
    uint64_t unk1;
    THashMap<ZString, STypeID *> types_by_name;
    uint64_t unk2;
    TArray<STypeID *> types_without_name;
    char unk3[164];
};

// TODO: Proper name from the game
struct SPropertyDataInner {
    void *unk1;
    uint64_t unk2; // Some kind of incremental index
    uint64_t unk3;
    void *unk4; // Some function
};

struct SPropertyData {
    uint32_t name_crc32;
    uint32_t unk1;
    uint32_t unk2;
    uint32_t unk3;
    SPropertyDataInner *inner;
};

struct SPinData {
    uint32_t unk1; // TODO: Maybe padding?
    void *activate_func;
    // Same as unk3 in TypeInfoInputPin?
    uint64_t activate_func_param1;
    uint64_t unk2;
    int64_t activate_func_param2_offset;
    uint64_t activate_func_param4;
};

struct ZEntityType {
    uint64_t unk1;
    TArray<SPropertyData> *properties;
    void **unk2;
    void **unk3;
    void **unk4;
    void **unk5;
    /** Pin name crc32 to SPinData */
    TArray<TPair<uint32_t, SPinData>> *input_pins;
    TArray<TPair<uint32_t, SPinData>> *output_pins;
};

struct ZVariantRef {
    STypeID *stypeid;
    void *value;
};

typedef void ZDebugConsole;
typedef void ZResourceManager;

struct ZResourceStub {
    void *vptr;
    uint64_t runtime_resource_id;
    uint32_t resource_status; // ?
    uint32_t resource_tag;
    void* install_result;
    uint64_t unk1;
};

struct ZResourceReader {
    void *vptr;
    ZResourceStub *resource_stub;
    // TODO: Maybe this actually points to some struct and isn't just a double
    // pointer 
    void **raw_bytes;
    // Pointed to some weird ResourceDataBuffer-derived typeinfo once
    void *unk1;
    /* The length of the data
     * TODO: With/without the header that can be stored separately? */
    uint32_t length;
};

struct ZResourcePending {
    ZResourceStub *resource_stub;
    ZResourceReader *resource_reader;
    int unk1;
    long unk2;
};

struct IResourceInstaller {
    struct {
        // There aren't proper types for some of the function pointer, because
        // they haven't been needed yet. When adding proper types, consult the
        // non-stripped Mac version for argument types
        void *destructor;
        // TODO: Only on some versions
        void *destructor2;
        // These are from IComponentInterface, abstract them?
        void *GetVariantRef;
        void *AddRef;
        void *Release;
        void *QueryInterface;
        void *Allocate;
        bool (*Install)(IResourceInstaller *self, ZResourcePending *);
        void *Release_2;
        void *IsStreamInstaller;
        void *IsIndirectionInstaller;
        void *SupportsAllocate;
        void *OnOrphanedResource;
        void *GetInstallerId;
    } *vptr;
};

#if !defined(_WIN32)
typedef void ID3D12Device;
typedef void IDXGISwapChain1;
#endif

namespace nx {
    class NxD3DImpl { };
    class NxDXGIImpl { };
}

class NxApp_Globals {
    public:
#if defined(_WIN32)
    void *app;
    void *resource_loader;
    void *assert;
    void *exceptions;
    void *nx_call_stack;
    void *log;
    void *settings;
    void *localization;
    void *monitor;
    nx::NxDXGIImpl *dxgi;
    nx::NxD3DImpl *direct3D;
    void *adl;
    void *nx_nvidia;
    void *swap_chain;
    void *steam;
    void *mini_dump;
    void *fail_dialog;
    void *game_window;
    void *input;
    void *razer_chroma;
    void *eye_tracker;
    void *actions;
    void *game_state;
    void *launcher;
    void *benchmark;
    void *config_dialog;
    void *scaleform;
#endif
};

extern std::function<void *(const char *name)> GetGlobalPointer;
extern std::function<void(ZDebugConsole *self, const char *cmd)> ZDebugConsole__ExecuteCommand;
extern std::function<void(ZString *transition)> TriggerMenuScreenTransition__string;
extern std::function<ID3D12Device *(nx::NxD3DImpl *)> NxD3DImpl__GetD3D12Device;
extern std::function<IDXGISwapChain1 *(nx::NxDXGIImpl *)> NxDXGIImpl__GetSwapChain;

extern ZDebugConsole *g_pDebugConsoleSingleton;
extern uint32_t *globalPointerCount;
extern GlobalPointer *globalPointers;
extern NxApp_Globals **s_Globals;
