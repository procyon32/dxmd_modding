// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "dehash.hpp"
#include <array>
#include <pahil/logging.hpp>

std::unordered_map<uint32_t, std::string> decrc32_map;
std::mutex decrc32_map_write_mutex;

constexpr std::array<uint32_t, 256> generate_crc32_table() {
    std::array<uint32_t, 256> table = {0};
    uint32_t polynomial = 0xEDB88320;
    for(size_t i = 0; i < 256; i++) {
        uint32_t c = i;
        for (size_t j = 0; j < 8; j++) {
            if (c & 1) {
                c = polynomial ^ (c >> 1);
            }
            else {
                c >>= 1;
            }
        }
        table[i] = c;
    }

    return table;
}

static std::array<uint32_t, 256> crc32_table = generate_crc32_table();

uint32_t compute_crc32(std::string str) {
    uint32_t crc32 = 0xFFFFFFFF;
    for(uint8_t c : str) {
        auto lookup_index = ((crc32 & 0xFF) ^ c) & 0xFF;
        crc32 = (crc32 >> 8) ^ crc32_table[lookup_index];
    }
    return ~crc32;
}

void decrc32_add_str(std::string str) {
    uint32_t crc32 = compute_crc32(str);
    std::lock_guard guard(decrc32_map_write_mutex);
    if(decrc32_map.find(crc32) != decrc32_map.end()
            && decrc32_map[crc32] != str) {
        LOG << "DeCrc32 colision! " << str << " and " << decrc32_map[crc32];
    }
    decrc32_map[crc32] = str;
}
