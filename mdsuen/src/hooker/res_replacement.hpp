// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <unordered_set>
#include <filesystem>

/** A cache for checking whether a resource with an RRID should be replaced */
extern std::unordered_set<uint64_t> replaced_resources;
/** The directory where replacement files will be searched for.
 *  The files' filenames should be the replaced RRIDs in hex */
extern std::filesystem::path replacement_resource_dir;

void load_replaced_resources_list();
