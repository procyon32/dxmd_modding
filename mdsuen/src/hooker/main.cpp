// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include <cstdlib>
#include <fstream>
#include <thread>
#include <pahil/logging.hpp>
#include "hooker/version.hpp"
#include "hooker/gui.hpp"

#if defined(__linux__)
#include <stdlib.h>
#include <dlfcn.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

/** Will be called before the hooked process' main() */
void on_library_load() {
    LOG << "MDSuen is starting...";
    try {
        auto version = find_game_version();
        if(!version) {
            LOG << "Couldn't find game version, exiting!";
            return;
        }
        LOG << "Found game version: " << version->name;
#if defined(__linux__)
        // Unset LD_PRELOAD so that MDSuen doesn't start in child processes
        unsetenv("LD_PRELOAD");
#endif
        version->on_hook();
    } catch(std::exception& e) {
        LOG << "MDSuen exception in init: " << e.what();
        return;
    }
    LOG << "MDSuen init complete!";
}

#if defined(__linux__)
struct LinuxInitHelper {
    LinuxInitHelper() {
        // This shouldn't be necessary, since iostream defines a static one,
        // but cout/... don't work without this line
        std::iostream::Init io_init;

        on_library_load();
    }
};

// The explicit priority is needed to have global objects initialised when the
// init function is run.
//__attribute__((init_priority(65535))) LinuxInitHelper linux_init_helper;
LinuxInitHelper linux_init_helper;
#elif defined(_WIN32)
bool WINAPI DllMain(HINSTANCE dll, int reason, void *reserved) {
    if(reason != DLL_PROCESS_ATTACH) {
        return true;
    }

    // It's hard to view the stdout/stderr of a GUI process on Windows, so
    // redirect the log into a file
    auto logfile = new std::ofstream(
        "mdsuen.log", std::ios::out | std::ios::trunc);
    pahil::logger.stream = logfile;

    on_library_load();
    return true;
}
#else
#error "Unsupported platform"
#endif
