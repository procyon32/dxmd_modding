// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "res_replacement.hpp"
#include <filesystem>
#include <pahil/logging.hpp>

std::unordered_set<uint64_t> replaced_resources;
std::filesystem::path replacement_resource_dir = "replacement_resources";

void load_replaced_resources_list() {
    if(!std::filesystem::exists(replacement_resource_dir)) {
        return;
    }
    for(auto& file : std::filesystem::directory_iterator(replacement_resource_dir)) {
        auto filename = file.path().filename().string();
        try {
            size_t endpos;
            auto rrid = std::stoul(filename, &endpos, 16);
            if(endpos != filename.size()) {
                LOG << "Invalid replacement resource filename: " << filename;
                continue;
            }
            replaced_resources.insert(rrid);
            LOG << "Registered replacement resource: " << std::hex << rrid;
        } catch(std::exception& e) {
            LOG << "Invalid replacement resource filename: " << filename
                << "(" << e.what() << ")";
        }
    }

    LOG << "Found " << replaced_resources.size() << " replacement resources";
}
