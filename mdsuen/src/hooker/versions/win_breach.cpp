// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "hooker/version.hpp"
#include <pahil.hpp>
#include "hooker/hooks.hpp"

GameVersion win_breach_version = {
    "dxmd_win_breach_758",
    44840448,
    []() {
        pahil::Module prog(0x140000000);
        prog.init_exec_allocator();
        prog.unprotect_pages();
        LOG << "Pages unprotected";

        {
            // This is an ugly hack that prevents a fatal exception caused by
            // out-of-bounds access of a bitset. This gets triggered while
            // setting up the input handling of all things. Is it just my
            // computer, what is the actual cause?
            auto p = prog.patch();
            p.data(0x140a6788f, {0x72}, {0xEB});
            p.apply();
        }

        {
            auto p = prog.patch();
            p.hook(0x140021150, reinterpret_cast<void *>(hook_WinMain));
            p.hook(0x14001ddc0, reinterpret_cast<void *>(hook_ZApplicationEngineWin32__MainLoop));
            p.hook(0x140017df0, reinterpret_cast<void *>(hook_ZEngineAppCommon__MainLoopSequence));
            p.apply();
        }

        {
            auto p = prog.patch();
            p.hook(0x140a13300, reinterpret_cast<void *>(hook_ZDebugConsole__AddLine));
            p.apply();
        }

        GetGlobalPointer = reinterpret_cast<void *(*)(const char *)>(prog.translate_addr(0x140036200));
        ZDebugConsole__ExecuteCommand = reinterpret_cast<void (*)(void *self, const char *cmd)>(prog.translate_addr(0x140a141d0));
        TriggerMenuScreenTransition__string = reinterpret_cast<void (*)(ZString *)>(prog.translate_addr(0x14136ff40));

        g_pDebugConsoleSingleton = reinterpret_cast<void *>(prog.translate_addr(0x142f05730));
        s_Globals = reinterpret_cast<NxApp_Globals **>(prog.translate_addr(0x142f684b8));
    },
};
