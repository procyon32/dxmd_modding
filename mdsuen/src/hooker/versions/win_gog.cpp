// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "hooker/version.hpp"
#include <pahil.hpp>
#include "hooker/hooks.hpp"

GameVersion win_gog_version = {
    "dxmd_gog_801",
    44747776,
    []() {
        pahil::Module prog(0x140000000);
        prog.init_exec_allocator();
        prog.unprotect_pages();
        LOG << "Pages unprotected";

        {
            auto p = prog.patch();
            p.hook(0x1400211b0, reinterpret_cast<void *>(hook_WinMain));
            p.hook(0x14001db20, reinterpret_cast<void *>(hook_ZApplicationEngineWin32__MainLoop));
            p.hook(0x1400178b0, reinterpret_cast<void *>(hook_ZEngineAppCommon__MainLoopSequence));
            p.apply();
        }

        {
            auto p = prog.patch();
            p.hook(0x140a14200, reinterpret_cast<void *>(hook_ZDebugConsole__AddLine));
            p.apply();
        }

        GetGlobalPointer = reinterpret_cast<void *(*)(const char *)>(prog.translate_addr(0x140036380));
        ZDebugConsole__ExecuteCommand = reinterpret_cast<void (*)(void *self, const char *cmd)>(prog.translate_addr(0x140a150d0));
        TriggerMenuScreenTransition__string = reinterpret_cast<void (*)(ZString *)>(prog.translate_addr(0x1413714e0));

        g_pDebugConsoleSingleton = reinterpret_cast<void *>(prog.translate_addr(0x142f06720));
    },
};
