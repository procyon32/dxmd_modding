// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <unordered_map>
#include <mutex>

extern std::unordered_map<uint32_t, std::string> decrc32_map;
extern std::mutex decrc32_map_write_mutex;

uint32_t compute_crc32(std::string str);
void decrc32_add_str(std::string str);
