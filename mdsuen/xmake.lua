add_repositories("dvdkon-repo git@gitlab.com:dvdkon/xmake-repo.git")
add_requires("args_hxx", "imgui", "imgui-overlay", "pahil")

set_warnings("all")
set_languages("cxx17")
if is_mode("debug") then
	set_symbols("debug")
	set_optimize("none")
end

if is_plat("windows") then
	add_cxxflags("/EHsc") -- Exceptions

	-- Necessary for asmjit
	add_defines("WIN32_LEAN_AND_MEAN", "NOMINMAX")
end

-- TODO: Move to a separate repo
target("bbdcd")
	set_kind("binary")
	add_files("bbdcd/**.cpp")

target("mdsuen_lib")
	set_kind("shared")
	add_files("src/hooker/*.cpp")
	add_files("src/hooker/versions/*.cpp")
	add_files("src/common/*.cpp")
	add_includedirs("src")
	-- Note that this order matters. because the order of static libraries
	-- matters (at least for clang/gcc)
    add_packages("imgui-overlay", "imgui", "pahil")
	if is_plat("windows") then
		add_syslinks("dxgi")
		add_syslinks("d3d11")
		add_syslinks("d3d12")
	elseif is_plat("linux") then
		-- For overlay package
		add_links("GLEW")
	end
	-- TODO: Separate debug and release modes?

target("mdsuen_launcher")
	set_kind("binary")
	add_files("src/launcher/*.cpp")
	add_files("src/common/*.cpp")
	add_includedirs("src")
	add_packages("args_hxx", "pahil")
