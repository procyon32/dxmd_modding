#!/usr/bin/env python3
import sys
import construct
import structs

construct.lib.setGlobalPrintFullStrings(True)

path = sys.argv[1]
h = structs.HLIB.parse_file(path)
#print(h)
hi_data = h.bin1.inner
#print(hi_data)
hi = structs.HLIBInner.parse(hi_data)
print(hi)
#for res in h.bin1.reslist.ress:
#    print(f"\nRES @{res.offset}/{res.res.offset}")
#    if res.res.data is not None:
#        print(f"\t{res.res.data.data}")
