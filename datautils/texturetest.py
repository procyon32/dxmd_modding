#!/usr/bin/env python3
import sys
import structs

data = open(sys.argv[1], "rb").read()
hdr = structs.TextureHeader.parse(data)
print(hdr)

def get_mip_size(format, full_width, full_height, mip_index):
    width = full_width
    height = full_height
    for i in range(mip_index):
        width = width + 1 >> 1
        height = height + 1 >> 1

    blocksize = 1
    if format >= 1 and format <= 4: bytes_per_block = 16
    elif format >= 5 and format <= 8: bytes_per_block = 12
    elif format >= 9 and format <= 22: bytes_per_block = 8
    elif (format >= 23 and format <= 32) \
        or (format >= 34 and format <= 51) \
        or format == 97: bytes_per_block = 4
    elif format >= 52 and format <= 65: bytes_per_block = 2
    elif format >= 66 and format <= 71: bytes_per_block = 1
    elif format == 72: bytes_per_block = 0
    elif (format >= 73 and format <= 75) \
        or (format >= 82 and format <= 84):
        bytes_per_block = 8
        blocksize = 4
    elif (format >= 76 and format <= 81) \
        or (format >= 85 and format <= 92):
        bytes_per_block = 16
        blocksize = 4
    elif format == 94: bytes_per_block = 4
    elif format == 95: bytes_per_block = 2
    else:
        raise Exception("Invalid format! " + str(format))

    return \
        ((width - 1 + blocksize) // blocksize) \
        * ((height - 1 + blocksize) // blocksize) \
        * bytes_per_block

size_cumm = 0
for ml in range(hdr.num_mip_levels):
    size = get_mip_size(hdr.format, hdr.width, hdr.height, ml)
    print(f"mip level {ml} size={size} offset={size_cumm}")
    size_cumm += size

print("Computed file size:", size_cumm + 0x24)
