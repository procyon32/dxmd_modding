#!/usr/bin/env python3
import sys
import structs

path = sys.argv[1]
a = structs.ARCH.parse_file(path)
#print(a)
print("-- Containing files:")
for f in a.metadata.containing_files:
    print(f.string)
print("-- Files:")
for f in a.metadata.files:
    print(f.name.string, f.unk1, f.unk2)
    for p in f.parts:
        print(f"    cf={p.containing_file} of_g={p.offset_global} of_l={p.offset_local} len={p.data_len}")
